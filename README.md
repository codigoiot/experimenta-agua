# Experimenta Agua

Este repositorio contiene los documentos de la estación de medición generada en el proyecto Experimenta Agua, donde CodigoIoT fue invitado por el Centro de Cultura Digital.

En este repositorio se muestra la versión 2 del proyecto. Para mas detalles, consulta el curso con las instrucciones para armar el dispositivo en https://edu.codigoiot.com/course/view.php?id=808
