// This #include statement was automatically added by the Particle IDE.
#include <Adafruit_DHT.h>

// This #include statement was automatically added by the Particle IDE.
#include <NeoGPS.h>

// This #include statement was automatically added by the Particle IDE.
#include <DS18B20.h>

// This #include statement was automatically added by the Particle IDE.
#include <OLED_Display_128X64.h>

//GPS
NMEAGPS  gps; // This parses the GPS characters
#define gpsPort Serial1
#if !defined( NMEAGPS_PARSE_RMC )
  #error You must uncomment NMEAGPS_PARSE_RMC in NMEAGPS_cfg.h!
#endif

#if !defined( NMEAGPS_PARSE_GGA )
  #error You must uncomment NMEAGPS_PARSE_GGA in NMEAGPS_cfg.h!
#endif

#if !defined( GPS_FIX_LOCATION )
  #error You must uncomment GPS_FIX_LOCATION in GPSfix_cfg.h!
#endif

#if !defined( GPS_FIX_ALTITUDE )
  #error You must uncomment GPS_FIX_ALTITUDE in GPSfix_cfg.h!
#endif

// DHT
#define DHTPIN 2
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

//Sensor de temperatura DS18B20
#include <math.h>
const int      MAXRETRY          = 4;
const uint32_t msSAMPLE_INTERVAL = 2500;
const uint32_t msMETRIC_PUBLISH  = 10000;
const int16_t dsVCC  = D2;
const int16_t dsData = D3;
const int16_t dsGND  = D4;
// Sets Pin D3 as data pin and the only sensor on bus
DS18B20  ds18b20(dsData, true); 
char     szInfo[64];
double   celsius;
double   fahrenheit;
uint32_t msLastMetric;
uint32_t msLastSample;

void setup() {
    
    //GPS
    Serial.begin(9600);
    while (!Serial)
      ;
    Serial.print( F("NMEAsimple.INO: started\n") );
    gpsPort.begin(9600);
    
    // DHT
    dht.begin();
    
    //Inicializar Pantalla
    Wire.begin();
    SeeedOled.init();  //initialze SEEED OLED display
    
    SeeedOled.clearDisplay();          //clear the screen and set start position to top left corner
    SeeedOled.setNormalDisplay();      //Set display to normal mode (i.e non-inverse mode)
    SeeedOled.setPageMode();           //Set addressing mode to Page Mode
    SeeedOled.setTextXY(0,0);          //Set the cursor to Xth Page, Yth Column 
    SeeedOled.putString("Experimenta Agua"); //Print the String
    delay (5000);
    
    //DS18B20
    pinMode(dsGND, OUTPUT);
    digitalWrite(dsGND, LOW);
    pinMode(dsVCC, OUTPUT);
    digitalWrite(dsVCC, HIGH);

}

void loop() {
    
    //Obtener coordenadas
    while (gps.available( gpsPort )) {
        gps_fix fix = gps.read();
        Serial.print( F("Location: ") );
        if (fix.valid.location)
        Serial.print( fix.latitude(), 6 ); // floating-point display
        Serial.print( ',' );
        if (fix.valid.location)
        Serial.print( fix.longitude(), 6 ); // floating-point display
        Serial.print( F(", Altitude: ") );
        if (fix.valid.altitude)
        Serial.print( fix.altitude() );
        
        Serial.println();
    }
    
    //DHT
    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a 
    // very slow sensor)
	float h = dht.getHumidity();
    // Read temperature as Celsius
	float t = dht.getTempCelcius();
    // Read temperature as Farenheit
	float f = dht.getTempFarenheit();
    // Check if any reads failed and exit early (to try again).
	if (isnan(h) || isnan(t) || isnan(f)) {
		Serial.println("Failed to read from DHT sensor!");
		return;
	}
    // Compute heat index
    // Must send in temp in Fahrenheit!
	float hi = dht.getHeatIndex();
	float dp = dht.getDewPoint();
	float k = dht.getTempKelvin();
	Serial.print("Humid: "); 
	Serial.print(h);
	Serial.print("% - ");
	Serial.print("Temp: "); 
	Serial.print(t);
	Serial.print("*C ");
	Serial.print(f);
	Serial.print("*F ");
	Serial.print(k);
	Serial.print("*K - ");
	Serial.print("DewP: ");
	Serial.print(dp);
	Serial.print("*C - ");
	Serial.print("HeatI: ");
	Serial.print(hi);
	Serial.println("*C");
	Serial.println(Time.timeStr());
	
	//Sensor de temperatura DS18B20
	if (millis() - msLastSample >= msSAMPLE_INTERVAL){
        getTemp();
      }
    
      if (millis() - msLastMetric >= msMETRIC_PUBLISH){
        Serial.println("Publishing now.");
        publishData();
      }

}


//Metodos para el sensor de temperatura DS18B20
void publishData(){
  sprintf(szInfo, "%2.2f", fahrenheit);
  Particle.publish("dsTmp", szInfo, PRIVATE);
  msLastMetric = millis();
}

void getTemp(){
  float _temp;
  int   i = 0;

  do {
    _temp = ds18b20.getTemperature();
  } while (!ds18b20.crcCheck() && MAXRETRY > i++);

  if (i < MAXRETRY) {
    celsius = _temp;
    fahrenheit = ds18b20.convertToFahrenheit(_temp);
    Serial.println(fahrenheit);
  }
  else {
    celsius = fahrenheit = NAN;
    Serial.println("Invalid reading");
  }
  msLastSample = millis();
}